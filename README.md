## 云雀消息websocket uniapp实现示例
## rtmp 推拉流uniapp 前端实现
服务端可下载服务直接用，也可以下载go源码二开。
1.下载源码
```
wget https://github.com/q191201771/lal/releases/download/v0.36.7/lal_v0.36.7_linux.zip

unzip lal_v0.36.7_linux.zip
cd lal_v0.36.7_linux
```
2.启动
```
./bin/lalserver -c ./conf/lalserver.conf.json
```
使用
1.推流
需要本地有一个文件:test.flv 

注意:test.flv的时长建议>=1分钟,不然还没打开播放器,推流已经结束了
```
ffmpeg -re -i test.flv -c:a copy -c:v copy -f flv rtmp://192.168.168.111:1935/live/test110
```
2.播放
官方建议使用ffplay播放
```
ffplay rtmp://192.168.168.111/live/test110
ffplay rtsp://192.168.168.111:5544/live/test110
ffplay http://192.168.168.111:8080/live/test110.flv
ffplay http://192.168.168.111:8080/hls/test110/playlist.m3u8
ffplay http://192.168.168.111:8080/hls/test110/record.m3u8
ffplay http://192.168.168.111:8080/hls/test110.m3u8
ffplay http://192.168.168.111:8080/live/test110.ts
```
官方地址 go语言服务端源码地址
```
https://github.com/q191201771/lal?tab=readme-ov-file
```
